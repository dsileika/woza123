import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ChatsPage } from '../pages/chats/chats';
import { Meteor } from 'meteor/meteor';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  rootPage: any;

  constructor(public platform: Platform,private statusBar: StatusBar,private splashScreen: SplashScreen) {
    this.rootPage = Meteor.user() ? ChatsPage : LoginPage;
    
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
