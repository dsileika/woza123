// import 'meteor-client-side';
 

import 'meteor-client';
// import 'accounts-base-client-side';
// import 'accounts-password-client-side'
 

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MeteorObservable } from 'meteor-rxjs';
import { Meteor } from 'meteor/meteor';
import { AppModule } from './app.module';

// platformBrowserDynamic().bootstrapModule(AppModule);

Meteor.startup(() => {
  const subscription = MeteorObservable.autorun().subscribe(() => {
 
    if (Meteor.loggingIn()) {
      return;
    }
 
    setTimeout(() => subscription.unsubscribe());
    platformBrowserDynamic().bootstrapModule(AppModule);
  });
});