export const DEFAULT_PICTURE_URL = 'assets/default-profile-pic.svg';
 
export interface Profile {
  name?: string;
  picture?: string;
  pictureId?: string;
}

export enum MessageType {
  TEXT = <any>'text',
  LOCATION = <any>'location',
  PICTURE = <any>'picture',
  LIKE = <any>'like',
  UNLIKE = <any>'unlike',
  TYPING = <any>'typing'
}
 
export interface Chat {
  _id?: string;
  title?: string;
  picture?: string;
  lastMessage?: Message;
  memberIds?: string[];
  typingUsers?: any;
}
 
export interface Message {
  _id?: string;
  chatId?: string;
  content?: string;
  createdAt?: Date;
  senderId?: string;
  type?: MessageType
  ownership?: string;
  likes?: number;
  likers?: any;
  unlikes?: number;
  unlikers?: any;
} 

export interface User extends Meteor.User {
  profile?: Profile;
}

export interface Location {
  lat: number;
  lng: number;
  zoom: number;
}

export interface Picture {
  _id?: string;
  complete?: boolean;
  extension?: string;
  name?: string;
  progress?: number;
  size?: number;
  store?: string;
  token?: string;
  type?: string;
  uploadedAt?: Date;
  uploading?: boolean;
  url?: string;
  userId?: string;
} 