import { Messages, Users, Chats  } from './collections';
import { MessageType, Profile } from './models'; 
import { check, Match } from 'meteor/check'; 
import { _ } from 'meteor/underscore'; 

const nonEmptyString = Match.Where((str) => {
  check(str, String);
  return str.length > 0;
});

const nonEmptyObject = Match.Where((obj) => { 
      check(obj, Array); 
      return obj.length > 0;
});

Meteor.methods({
  addChat(receiverId: Array<Object>): void {
    if (!this.userId) {
      throw new Meteor.Error('unauthorized',
        'User must be logged-in to create a new chat');
    }

    check(receiverId, nonEmptyObject);

    receiverId.forEach(element => { 
        if (element === this.userId) {
          throw new Meteor.Error('illegal-receiver',
            'Receiver must be different than the current logged in user');
        }
    });


    receiverId.push(this.userId);
    
    const chatExists = !!Chats.collection.find({
      memberIds: { $all: receiverId }
    }).count();

    if (chatExists) {
      throw new Meteor.Error('chat-exists',
        'Chat already exists');
    }
    

    const chat = {
      memberIds: <string[]>receiverId,
      title: "Random chat"
    };

    Chats.insert(chat); 

  },
  removeChat(chatId: string): void {
    if (!this.userId) {
      throw new Meteor.Error('unauthorized',
        'User must be logged-in to remove chat');
    }

    check(chatId, nonEmptyString);

    const chatExists = !!Chats.collection.find(chatId).count();

    if (!chatExists) {
      throw new Meteor.Error('chat-not-exists',
        'Chat doesn\'t exist');
    }

    Chats.remove(chatId);
  },
  updateProfile(profile: Profile): void {
    if (!this.userId) throw new Meteor.Error('unauthorized',
      'User must be logged-in to create a new chat');

    check(profile, {
      name: nonEmptyString,
      pictureId: Match.Maybe(nonEmptyString)
    });

    Meteor.users.update(this.userId, {
      $set: {profile}
    });
  },
  userTyping(type: MessageType, chatId: string, isTyping: boolean){
    if (!this.userId) throw new Meteor.Error('unauthorized',
      'User must be logged-in to create a new chat');
    // check(type, Match.OneOf(String, [ MessageType.TYPING ]));
    check(chatId, nonEmptyString);
    check(this.userId, String);  
    check(isTyping, Boolean);
    
    if(isTyping === true){ 
      Chats.collection.update({_id: chatId}, {$addToSet: {typingUsers: this.userId}});
    } else { 
      Chats.collection.update({_id: chatId}, {$pull: {typingUsers: this.userId}});
    } 

  },
  addMessage(type: MessageType, chatId: string, content: string) {
    if (!this.userId) throw new Meteor.Error('unauthorized',
      'User must be logged-in to create a new chat');

    check(type, Match.OneOf(String, [ MessageType.TEXT, MessageType.LOCATION ]));
    check(chatId, nonEmptyString);
    check(content, nonEmptyString);

    const chatExists = !!Chats.collection.find(chatId).count();

    if (!chatExists) {
      throw new Meteor.Error('chat-not-exists',
        'Chat doesn\'t exist');
    }

    return {
      messageId: Messages.collection.insert({
        chatId: chatId,
        senderId: this.userId,
        content: content,
        createdAt: new Date(),
        type: type
      })
    };
  },
  likeMessage(type: MessageType, chatId: string, msgId: string){
    if (!this.userId) throw new Meteor.Error('unauthorized',
      'User must be logged-in to create a new chat');
    
    check(type, Match.OneOf(String, [ MessageType.LIKE ]));
    check(chatId, nonEmptyString);
    check(msgId, nonEmptyString);

     const chatExists = !!Chats.collection.find(chatId).count();

    if (!chatExists) {
      throw new Meteor.Error('chat-not-exists',
        'Chat doesn\'t exist');
    }
    
    var message = Messages.collection.findOne({ _id: msgId});
 
    if( message ) {
      if (_.contains(message.likers, this.userId)) {
          // FlashMessages.sendError("You already liked this recipe", {hideDelay: 1000});
          Messages.collection.update({ _id: msgId}, {$pull: {likers: this.userId}, $inc: {likes: -1}});
          return;
 
      }
      if(_.contains(message.unlikers, this.userId)){
         Messages.collection.update({ _id: msgId}, {$pull: {unlikers: this.userId}, $inc: {unlikes: -1}});
      }
      Messages.collection.update({ _id: msgId}, {$addToSet: {likers: this.userId}, $inc: {likes: 1}});
    } 

    return Messages.collection.find({ _id: msgId}).count();


  },
  unlikeMessage(type: MessageType, chatId: string, msgId: string){
    if (!this.userId) throw new Meteor.Error('unauthorized',
      'User must be logged-in to create a new chat');
    
    check(type, Match.OneOf(String, [ MessageType.UNLIKE ]));
    check(chatId, nonEmptyString);
    check(msgId, nonEmptyString);

     const chatExists = !!Chats.collection.find(chatId).count();

    if (!chatExists) {
      throw new Meteor.Error('chat-not-exists',
        'Chat doesn\'t exist');
    }
    
    var message = Messages.collection.findOne({ _id: msgId});
 
    if( message ) {
      if (_.contains(message.likers, this.userId)) {
          // FlashMessages.sendError("You already liked this recipe", {hideDelay: 1000});
         
          Messages.collection.update({ _id: msgId}, {$pull: {likers: this.userId}, $inc: {likes: -1}});
          // return;
      }
      if(_.contains(message.unlikers, this.userId)){
         Messages.collection.update({ _id: msgId}, {$pull: {unlikers: this.userId}, $inc: {unlikes: -1}});
         return;
      }
      Messages.collection.update({ _id: msgId}, {$addToSet: {unlikers: this.userId}, $inc: {unlikes: 1}});
    } 

    return Messages.collection.find({ _id: msgId}).count();


  },
  countMessages(): number {
    return Messages.collection.find().count();
  }
});